angular
.module("desafioSenior.service", [])
.factory("consultaService", ['$http', function($http){
  $public = {};
  $private = {pessoas: [], checkin: []};

  $public.getPessoas = function(callback){
      if($private.pessoas.length == 0){
          $http.get("pessoas.json").then(function (response) {
              $private.pessoas = response.data;
              callback(response.data);
          },
          function (data, status) {
              alert("Erro ao carregar dados");
          });
      }
      else{
          callback($private.pessoas);
      }
  };

  $public.adicionarPessoa = function (pessoa) {
      $private.pessoas.data.push(pessoa);
  };

  $public.adicionarCheckin = function (checkin) {
      var temChecking = false;
      for (var i = 0; i < $private.checkin.data.length; i++) {
          if ($private.checkin.data[i].pessoa.documento == checkin.pessoa.documento) {
              $private.checkin.data[i] = checkin;
              temChecking = true;
          }
      }

      if (!temChecking) {
          $private.checkin.data.push(checkin);
      }
  };

  $public.getCheckin = function(callback){
      if ($private.checkin.length == 0) {
          $http.get("checkin.json").then(function(response){
              $private.checkin = response.data;
              callback(response.data);
          });
      }
      else {
          callback($private.checkin);
      }

  };

  return $public;

}]);
