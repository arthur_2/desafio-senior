angular
.module("desafioSenior.controller", ['desafioSenior.service'])
.controller("desafioSeniorCtrl", ['$scope', 'consultaService','$http', '$state', function($scope, consultaService, $http, $state){
    $scope.app = "Desafio Sênior";
    $scope.pagination = {currentPage: 1, pageSize: 2};

    $scope.checkin = {};
    $scope.checkins = [];
    $scope.pessoas = [];

    $scope.carregaPessoas = function () {
        consultaService.getPessoas(function(result){
            $scope.pessoas = result.data;
        });
    };

    $scope.carregaChekin = function (checkin) {
        var checkinTemp = angular.copy(checkin);
        $scope.checkin = checkinTemp;
    };
    $scope.carregaCheckings = function () {
        consultaService.getCheckin(function(result){
            $scope.checkins = result.data;
        });
    };

    $scope.adicionarPessoa = function (pessoa) {
        consultaService.adicionarPessoa(angular.copy(pessoa));
        $state.go('home');
    };

    $scope.adicionarCheckin = function () {
        var checkinTemp = angular.copy($scope.checkin);
        consultaService.adicionarCheckin(checkinTemp);
        $scope.carregaCheckings();
        $scope.checkin = {};
    };

    $scope.carregaCheckings();
    $scope.carregaPessoas();
}]);
